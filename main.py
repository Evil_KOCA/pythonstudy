print("hello")
print(2+5)
print(2343434*509384567)
print(3+5*4)
print((3+5)*4)
print(11111*1111111)
print(40//8)
print(42//8)
print(42 % 8)
print(239 % 10)
print(2 ** 5)
print(2014 ** 14)
#бинарные - два операнда, унарные - один операнд
print(0.5+0.3)
print(1/3)
print(0.3+0.3+0.3)
print(9 ** 0.5)
print(5e-1)
print(1.2345e3)
print(1.2345e-3)
print(2014.0 ** 14)
print(7/3)
print(7//3)
print(int(2.99))
print(int(-1.6))
print(float(10))
print(9**19 - int(float(9**19)))
print(type(7.0))
a = 2
a += 3
a = a+3
a -= 3
a *= 3
a /= 3
a %= 3
a **= 4
print(a)
#Строчные и прописные
#должно начинаться с буквы или подчеркивания
#не должно являться ключевым словом(True)
#Регистр ИМЕЕТ значение
a = 2
a = 'adsfasdf'
#Динамическая типизация
input() #Читается с клавиатуры
input('Введите данные')
s = input()
a = int(input())
print('some output')
print(a, s)