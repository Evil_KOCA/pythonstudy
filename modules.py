# import sys
# import subprocess
#
#
# print(len(sys.argv))
# subprocess.call(['python','-h'])


# import math
# r = float(input())
# p = math.pi * r * 2
# print(p)


# import sys
# for x in range(1, len(sys.argv)):
#     print(sys.argv[x], end=' ')

import requests
r = requests.get('http://example.com/') # Простой GET запрос
print(r.text) # вывод ответа от сервера

par = {'key1': 'value1', 'key2': 'value2'} # задание параметров отдельно от URL
r = requests.get('http://example.com/', params=par) # GET запрос с параметрами
print(r.url) # Сформированный url-адрес с учетом параметров GET запроса
print(r.content) # Сформированный url-адрес с учетом параметров GET запроса


url = 'http://httpbin.org/cookies'
cookies = {'cookies_are': 'working'}
r = requests.get(url, cookies=cookies) # Отправка сформированных cookies на сервер
print(r.text)
print(r.cookies['example_cookie_name']) # использование cookies, полученных от сервера