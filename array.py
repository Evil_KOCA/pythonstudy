# students = ['Ivan', 'Masha', 'Sasha']
# teachers = ['Oleg', 'Alex']
# for student in students:
#     print("Hello, " + student + '!')
# print(len(students))
# #  индексация работает, как строками
# print(students + teachers)
# print(teachers * 4)
# students[1] = 'Oleg'
# print(students)
# students = ['Ivan', 'Masha', 'Sasha']
# students.append('Olga')
# print(students)
# students += 'Olga'  # ['Ivan', 'Masha', 'Sasha', 'Olga', 'O', 'l', 'g', 'a']
# print(students)
# students += ['Boris', 'Sergei']
# print(students)
# students.insert(1, 'Olga')
# print(students)
# students = ['Ivan', 'Masha', 'Sasha']
# students.remove('Sasha')
# del students[0]
# print(students)
# students = ['Ivan', 'Masha', 'Sasha']
# if 'Ivan' in students:
#     print('Here')
# if 'Abba' not in students:
#     print('Not Here')
# ind = students.index('Sasha')
# print(ind)
# # ind = students.index('Ann')
# # print(ind)
#
# students = ['Ivan', 'Masha', 'Sasha']
# print(min(students))
# print(max(students))
# ordered_students = list(sorted(students))
# print(ordered_students)
# print(list(students.reverse()))
# print(reversed(students))


# a = [1, 'A', 2]
# b = a
# print(a, b)
# a[0] = 42
# print(a, b)
#
# a = [1, 2, 3]
# b = a
# print(b)
# a[1] = 10
# print(b)
# b[0] = 20
# print(a)
# a = [5, 6]
# print(b)
#
# a = [0] * 5
# print(a)
# a = [0 for i in range(5)]
# print(a)
# a = [i * i for i in range(5)]
# print(a)


# a = [int(i) for i in input().split()]
# sum = 0
# for i in a:
#     sum += i
# print(sum)
# print(len(a))

# a = [int(i) for i in input().split()]
# if len(a) == 1:
#     for j in a:
#         print(j, end='')
# if len(a) == 2:
#     b = [a[1] * 2, a[0] * 2]
#     print(str(b[0]) + ' ' + str(b[1]))
# if len(a) != 1 and len(a) != 2:
#     c = []
#     for i in range(0, len(a)-1):
#         c = c + [a[i-1] + a[i+1]]
#     c = c + [a[0] + a[-2]]
#     for j in range(0, len(c)-1):
#         print(c[j], end=' ')
#     print(c[-1])


# a = [int(i) for i in input().split()]
# b = sorted(a)
# c = []
# if len(a) == 1:
#     print('')
# elif b[0] == b[-1]:
#     print(b[0])
# else:
#     b = b + [b[-1] * 2 + 1]
#     for i in range(0, len(b) - 1):
#         if (b.count(b[i])) > 1 and b[i] != b[i + 1]:
#             c = c + [b[i]]
#     for j in c:
#         print(j, end=' ')
a = [int(i) for i in input().split()]
for j in a:
    print(j, end=' ')
