# Задача 1
# a = ''
# with open(r'C:\Users\n.kan\dataset_3363_2.txt', 'r') as inf:
#     for line in inf:
#         a = line.strip()
# q = set()
# for i in range(10):
#     q.add(str(i))
# n = []
# l = []
# j = 0
# ind = 'str'
# for i in a:
#     if i.isalpha():
#         l = l + [i]
#         ind = 'str'
#     elif i.isdigit() and ind == 'str':
#         n = n + [i]
#         ind = 'dig'
#     elif i.isdigit() and ind == 'dig':
#         n[-1] = n[-1] + i
# nint = []
# for i in range(len(n)):
#     nint.append(int(n[i]))
# result = ''
# for i in range(len(l)):
#     result = result + l[i] * nint[i]
# with open(r'C:\Users\n.kan\result.txt', 'w') as ouf:
#     ouf.write(result)

# Задача 2

# def max_string(li):
#     a = li[0]
#     for i in li:
#         if i > a:
#             a = i
#     return a
#
# d = [] # список под все слова, использованные в файле
# with open(r'C:\Users\n.kan\dataset_3363_3.txt', 'r') as inf:
#     for line in inf:
#         d = d + line.lower().split()
# print(d)
# print('Всего слов ', len(d))
# s = set() # Множество из всех слов для словаря
# q = {} #
# for i in d:
#     s.add(i)
# print('Уникальных слов', len(s))
# for i in s:
#     q[i] = d.count(i)
# t =[]
# for value in q.values():
#     t = t + [value]
# max = max(t)
# t =[]
# for key, value in q.items():
#     if value == max:
#         t = t + [key]
# print(t)
# a = max_string(t)
# print(a, max)

# Задача №3

raw = []
with open(r'C:\Users\n.kan\dataset_3363_4.txt', 'r') as inf:
    for line in inf:
        raw = raw + [line.strip().split(';')]
print(raw)
m = len(raw)
print('m = ', m)
for i in raw:
    i[1] = int(i[1])
    i[2] = int(i[2])
    i[3] = int(i[3])
print(raw)
q = []
x = [0, 0, 0]
for i in raw:
    q = q + [(i[1] + i[2] + i[3]) / 3]
    x[0] = x[0] + i[1]
    x[1] = x[1] + i[2]
    x[2] = x[2] + i[3]
print(q)
print(x)
print(m)
for i in range(3):
    x[i] = x[i]/m
print(x)
with open(r'C:\Users\n.kan\dataset_3363_4_result.txt', 'w') as wr:
    for i in q:
        wr.write(str(i))
        wr.write('\n')
    for i in x:
        wr.write(str(i))
        wr.write(' ')
    wr.write('\n')