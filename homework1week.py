# Task 1
# a, b, c = [int(input()), int(input()), int(input())]
#
# p = (a + b + c) / 2
# S = (p * (p - a)*(p - b)*(p - c)) ** 0.5
# print(S)


# Task 2
# c = int(input())
# a = -15 < c <= 12 or 14 < c < 17 or 19 <= c
# print(a)


# Task 3
# a, b = [float(input()), float(input())]
# c = input()
#
# if c == '+':
#     print(a + b)
# elif c == "-":
#     print(a - b)
# elif c == "/":
#     if b == 0:
#         print('Деление на 0!')
#     else:
#         print(a / b)
# elif c == "*":
#     print(a * b)
# elif c == "mod":
#     if b == 0:
#         print('Деление на 0!')
#     else:
#         print(a % b)
# elif c == "pow":
#     print(a ** b)
# elif c == "div":
#     if b == 0:
#         print('Деление на 0!')
#     else:
#         print(a // b)


# Task 4
# c = input()
# if c == 'треугольник':
#     a, b, c = [float(input()), float(input()), float(input())]
#     p = (a + b + c) / 2
#     S = (p * (p - a)*(p - b)*(p - c)) ** 0.5
#     print(S)
# elif c == 'прямоугольник':
#     a, b = [float(input()), float(input())]
#     S = a * b
#     print(S)
# elif c == 'круг':
#     c = float(input())
#     S = 3.14 * c ** 2
#     print(S)


# Task 5
# a, b, c = [int(input()), int(input()), int(input())]
# if a >= b >= c:
#     print(a)
#     print(c)
#     print(b)
# elif a >= c >= b:
#     print(a)
#     print(b)
#     print(c)
# elif b >= a >= c:
#     print(b)
#     print(c)
#     print(a)
# elif b >= c >= a:
#     print(b)
#     print(a)
#     print(c)
# elif c >= a >= b:
#     print(c)
#     print(b)
#     print(a)
# elif c >= b >= a:
#     print(c)
#     print(a)
#     print(b)

# Task 6
# n = int(input())
# a = n % 10
# b = n % 100
# if 11 <= b <= 19:
#     print(n, 'программистов')
# elif a == 1:
#     print(n, 'программист')
# elif 2 <= a <= 4:
#     print(n, 'программиста')
# else:
#     print(n, 'программистов')

# Task 7
n = int(input())
k = n // 1000
a = k % 10 + k // 100 + (k % 100 - k % 10) // 10
k = n % 1000
b = k % 10 + k // 100 + (k % 100 - k % 10) // 10
if a == b:
    print('Счастливый')
else:
    print('Обычный')
