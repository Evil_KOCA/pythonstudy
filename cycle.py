# i = 5
# S = 0
# while i != 0:
#     i = int(input())
#     S = S + i
# print(S)

# a, b = int(input()), int(input())
# d = 1
# i = 0
# while i != 5:
#     if d % a == 0 and d % b == 0:
#         print(d, end=' ')
#         i = 5
#     else:
#         d = d + 1

# i = 0
# while i < 5:
#     a, b = input().split()
#     a = int(a)
#     b = int(b)
#     if a == 0 and b == 0:
#         break
#     if a == 0 or b == 0:
#         continue
#     print(a*b)
#     i += 1

# while 1 == 1:
#     a = int(input())
#     if a > 100:
#         break
#     if a < 10:
#         continue
#     else:
#         print(a)

# i = 0
# s = 0
# while i < 10:
#     i = i + 1
#     s = s + i
#     if s > 15:
#         print(i)
#         continue
#     i = i + 1
# a = 5
# while a > 0:
#     print(a, end=' ')
#     a -= 1
#
# print('\n')
# a = 5
# while a <= 55:
#     print(a, end=' ')
#     a += 2
#
#
# print('\n')
# c = 1
# while c <= 6:
#     print('*' * c)
#     c += 1
#
# print('\n')
# i = 0
# while i < 5:
#     print('*')
#     if i % 2 == 0:
#         print('**')
#     if i > 2:
#         print('***')
#     i = i + 1
#
# print('\n')
# a, b = int(input()), int(input())
# s = 0
# i = a
# while i <= b:
#     s += i
#     i += 1
# print(s)

# for i in 2, 3, 5:
#     print(i*i)
# for i in range(10):
#     print(i*i)
#
# for i in range(5):
#     print(i)
# for i in range(2, 5):
#     print(i)
# for i in range(2, 15, 4): # от 2 до 15 с шагом 4, не включая 15
#     print(i)
# n = int(input())
# for i in range(n):
#     print('*' * n)

# n = int(input())
# for i in range(n):
#     for j in range(n):
#         print('*', end='\t')
#     print()

# a, b, c, d = int(input()), int(input()), int(input()), int(input())
# a, b, c, d = 3, 6, 7, 9
# i, j = 0, 0
# for i in range(a, b + 1):
#     for j in range(c, d + 1):
#         print(i * j, end='\t')
#     print()


# a, b, c, d = int(input()), int(input()), int(input()), int(input())
# print("  ", end='\t')
# for i in range(c, d+1):
#     print(i, end='\t')
# print()
# for i in range(a, b + 1):
#     print(i, end='\t')
#     for j in range(c, d + 1):
#         print(i * j, end='\t')
#     print()

# a, b = (int(i) for i in input().split())

a, b = int(input()), int(input())
n = 0
s = 0
while a % 3 != 0:
    a = a + 1
for i in range(a, b + 1, 3):
    s = s + i
    n = n + 1
print(s / n)
# https://stepik.org/lesson/3367/step/1?unit=950
