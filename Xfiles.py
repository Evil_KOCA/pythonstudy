import os

inf = open('file.txt', 'r') # open('file.txt')
s1 = inf.readline()
s2 = inf.readline()
inf.close() # закрытие файла

with open('text.txt') as inf:
    s1 = inf.readline()
    s2 = inf.readline()
# Здесь файл уже закрыт

s = inf.readline().strip()
'\t abc \n'.strip()  # == 'abc'

os.path.join('.', 'dirname', 'filename.txt')
'./dirname/filename.txt'

# Построчное чтение файла
with open('input.txt') as inf:
    for line in inf:
        line = line.strip()
        print(line)

# запись в файл
ouf = open('file/txt', 'w')
ouf.write('Some text\n')
ouf.write(str(25))
ouf.close()

with open('text.txt', 'w') as ouf:
    ouf.write(('Some text\n'))
    ouf.write(str(25))

# Здесь файл уже закрыт

