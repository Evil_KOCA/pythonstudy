# print('Variables Lesson')
# a = int(input('Enter number '))
# print(a * 2)
# print('Сколько минут ты хочешь поспать, Коля? ')
# T = int(input())
# print(T // 60)
# print(T % 60)

# X = int(input())
# H = int(input())
# M = int(input())

# T = X + H * 60 + M
# print(T // 60)
# print(T % 60)


# Логические операции
x = True
y = False
print(x or y)
print(x and y)
print(not x)
print(5 < 7)

x1, x2, x3 = False, True, False
print(not x1 or x2 and x3)

x = 5
y = 10
print(y > x * x or y >= 2 * x and x < y)
print(10 > 25 or 10 >= 10 and 5 < 10)

a = True
b = False
print(a and b or not a and not b)

if x % 2 == 0:
    print('Even')  # elif
else:
    print('Odd')
