s = set() # создание пустого множества
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
print(basket)
print('orange' in basket)
print('python' in basket)
print('python' not in basket)

for i in basket:
    print(i)

s.add('Tree')
basket.remove('apple') # Если элемента не будет то ошибка
s.discard('apple') # Если элемента не будет, то ошибки не будет
s.clear() # Сделать множество пустым - удалить все элементы
print(len(s))


dict, {} # пустой словарь
d = {'a': 239, 10: 100}
print(d['a'])
print(d[10])

print('get over here')
print(type(d))
print(10 in d.keys())
print('a' in d)
print(11 not in d)
print(d[10] == 101)
d['b'] = 102
d['c'] = 1001
print(d)
print(d.get(10))
del d['c']
print(d)

# Перебор элементов словаря
k = {'C': 14, 'A': 12, 'T': 9, 'G': 18}
for key in k:
    print(key, end=' ') # G C A T
print()
for key in k.keys():
    print(key, end=' ') # G C A T
print()
for value in k.values():
    print(value, end=' ') # 18 14 12 9
print()
for key, value in k.items():
    print(key, value, end='; ') # G 18; C 14; A 12; T 9;
print()
for item in k.items():
    print(item, end=' ')
k['C'] = ['KARRR', 'URRRA']
print(k['C'])