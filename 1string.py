# genome = 'ATGG'
# print(genome[0])
# for i in range(0,4):
#     print(genome[i], end=" ")
# for c in genome:
#     print(c)
#
# genome = input()
# cnt = 0
# for nucl in genome:
#     if nucl == 'C':
#         cnt += 1
# print(cnt)
#
# genome = input()
# print(genome.count('C'))


# s = 'aTGcc'
# p = 'cc'
# print(s.upper())  # перевод в ВЕРХНИЙ РЕГИСТР
# print(s.lower())    # перевод в нижний регистр
# print(s.count(p))   # выводит сколько раз символ 'p' встречается в s
# print(s.find(p))   # первое вхождение(индекс) p в s
# print(s.find("A"))   # строка 'A' не входит в s. Проверка вхождения в строку: if "TG" in s:
# print(s.replace('c', 'C'))   # заменяем все вхождения 'c' на 'C'
# s = 'agTtcAGtc' # последовательный вызов методов
# print(s.upper().count("gt".upper()))
# print(len(s))


# a = input()
# b = 100 * (a.lower().count('g') + a.lower().count('c')) / len(a)
# print(b)


# dna = 'ATTCGAGCT'
# print(dna[1])
# print(dna[1:4])
# print(dna[:4])
# print(dna[4:])
# print(dna[-4])
# print(dna[1:-1])
# print(dna[1:-1:2])
# print(dna[::-1])  # обратный порядок
# print(dna[1])
#
# pal = 'казак'
# if pal == pal[::-1]:
#     print('Палиндром')
# else: print('Рикошет')

# s = "abcdefghijk"
# print(s[3:6])
# print(s[:-6])
# print(len(s))


# seq = 'aaaabbcaa'
# seq = input()
# newseq = seq[0]
# cnt = 1
# if len(seq) == 1:
#     print(seq + '1')
# else:
#     for i in range(1,len(seq)):
#         if seq[i] == seq[i-1]:
#             cnt = cnt + 1
#         if seq[i] != seq[i-1]:
#             newseq = newseq + str(cnt) + seq[i]
#             cnt = 1
#         if i == len(seq) - 1:
#             newseq = newseq + str(cnt)
#     print(newseq)