# def min2(a, b):
#     if a <= b:
#         return a
#     else:
#         return b
#
#
# def min(*a):  # звездочка(*) указывает, что функция принимает произвольное число аргументов
#     m = a[0]
#     for x in a:
#         if m > x:
#             m = x
#     return m
#
#
# def my_range(start, stop, step=1):
#     res = []
#     if step > 0:
#         x = start
#         while x < stop:
#             res += [x]
#             x += step
#     elif step < 0:
#         x = start
#         while x > stop:
#             res += [x]
#             x += step
#     return res
#
#
# x = 0
#
#
# def f(x):
#     if x <= -2:
#         return (1 - (x + 2) ** 2)
#     elif -2 < x <= 2:
#         return (-(x / 2))
#     elif 2 < x:
#         return ((x - 2) ** 2 + 1)


#
#
# print(f(1))
#
# print(my_range(2, 5))
# print(my_range(2, 15, 3))
# print(my_range(15, 2, -3))
# print()
#
# print(min(3, 4, 5, 6, 7, 6, 5, 6, 123, -5))
# print(min2(42, 30))
# print(min2(min2(42, 50), 25))
#
# print()
# print(min(5))
# print(min(5, 3, 6, 10))
# print(min([5, 3, 6, 10], [4, 3, 6, -5]))
#
# a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# print(a)
# del a[2]
# print(a)
# a.remove(4)
# print(a)


def modify_list(l):
    a = len(l)
    q = []
    for i in range(a):
        if l[i] % 2 == 1:
            q = q + [i]
    for i in q[::-1]:
        del l[i]
    a = len(l)
    for i in range(a):
        l[i] = l[i] // 2





lst = [1, 2, 3, 4, 5, 6, 7, 8, 22]
print(lst)
modify_list(lst)
print(lst)
modify_list(lst)
print(lst)
modify_list(lst)
print(lst)
modify_list(lst)
print(lst)